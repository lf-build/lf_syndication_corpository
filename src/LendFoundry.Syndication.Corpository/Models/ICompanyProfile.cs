﻿namespace LendFoundry.Syndication.Corpository
{
    public interface ICompanyProfile
    {
        string AthorisedCapital { get; set; }
        string Cin { get; set; }
        string City { get; set; }
        string ClassOfCompany { get; set; }
        string CompanyCategory { get; set; }
        string CompanyName { get; set; }
        string CompanySubCategory { get; set; }
        string CompanyType { get; set; }
        string DateOfIncorporation { get; set; }
        string Email { get; set; }
        string IndianOrForeign { get; set; }
        string ListingStatus { get; set; }
        string PaidUpCapital { get; set; }
        string RegisteredAddress { get; set; }
        string RegisteredAddressMap { get; set; }
        string State { get; set; }
        string Status { get; set; }
    }
}
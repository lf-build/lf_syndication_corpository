﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Corpository
{
    public interface ISearchCompanyResponse
    {
       
        List<Company> Companies { get; set; }
        string Message { get; set; }
        SearchSummary SearchSummary { get; set; }
        int Status { get; set; }
    }
}
﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Corpository.Proxy.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Corpository
{
    public class SearchCompanyHistoryResponse : ISearchCompanyHistoryResponse
    {
        public SearchCompanyHistoryResponse() { }
        public SearchCompanyHistoryResponse(SearchCompanyHistoryProxyResponse companyResponse)
        {
            if (companyResponse == null)
                return;
            Status = companyResponse.Status;
            Message = companyResponse.Message;
            if (companyResponse.Companies != null)
            {
                Companies = new List<IHistoryCompany>(companyResponse.Companies.Select(company => new HistoryCompany(company)));
            }

        }
        public int Status { get; set; }

        public string Message { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IHistoryCompany, HistoryCompany>))]
        public List<IHistoryCompany> Companies { get; set; }


    }
    public class HistoryCompany : IHistoryCompany
    {
        public HistoryCompany() { }
        public HistoryCompany(ProxyHistoryCompany company)
        {
            CompanyId = company.CompanyId;
            LastRefreshTime = company.LastRefreshTime;
            LastDownloadedTime = company.LastDownloadedTime;
            CompanyProfile = new CompanyProfile(company.CompanyProfile);
            if (company.Signatories != null && company.Signatories.Any())
            {
                Signatories = new List<ISignatory>(company.Signatories.Select(Signatory => new Signatory(Signatory)));
            }
            CompanyProfilePaid = new CompanyProfilePaid(company.CompanyProfilePaid);
            if (company.FinancialSnapshotHistory != null && company.FinancialSnapshotHistory.Any())
            {
                FinancialSnapshotHistory = new List<IFinancialSnapshotHistory>(company.FinancialSnapshotHistory.Select(snapshot => new FinancialSnapshotHistory(snapshot)));
            }
        }
        public int CompanyId { get; set; }

        public long LastRefreshTime { get; set; }

        public long LastDownloadedTime { get; set; }
        public string ClassOfCompany { get; set; }

        public string ListingStatus { get; set; }

        public string CompanyCategory { get; set; }

        public string CompanySubCategory { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ICompanyProfile, CompanyProfile>))]
        public ICompanyProfile CompanyProfile { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISignatory, Signatory>))]
        public List<ISignatory> Signatories { get; set; }
        //[JsonProperty("charges")]
        //public List<Charge> charges { get; set; }

        public List<object> ConsolidatedFinancialMaster { get; set; }

        public List<object> ConsolidatedFinancialMasterHistories { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ICompanyProfilePaid, CompanyProfilePaid>))]
        public ICompanyProfilePaid CompanyProfilePaid { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IFinancialSnapshotHistory, FinancialSnapshotHistory>))]
        public List<IFinancialSnapshotHistory> FinancialSnapshotHistory { get; set; }


    }

    public class CompanyProfile : ICompanyProfile
    {
        public CompanyProfile() { }
        public CompanyProfile(ProxyCompanyProfile profile)
        {
            CompanyName = profile.CompanyName;
            Cin = profile.Cin;
            RegisteredAddress = profile.RegisteredAddress;
            City = profile.City;
            State = profile.State;
            RegisteredAddressMap = profile.RegisteredAddressMap;
            Email = profile.Email;
            ClassOfCompany = profile.ClassOfCompany;
            ListingStatus = profile.ListedOrUnlisted;
            Status = profile.Status;
            CompanyType = profile.CompanyType;
        }
        public string CompanyName { get; set; }

        public string Cin { get; set; }

        public string RegisteredAddress { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string RegisteredAddressMap { get; set; }

        public string Email { get; set; }

        public string ClassOfCompany { get; set; }

        public string ListingStatus { get; set; }

        public string CompanyCategory { get; set; }

        public string CompanySubCategory { get; set; }

        public string AthorisedCapital { get; set; }

        public string PaidUpCapital { get; set; }

        public string DateOfIncorporation { get; set; }

        public string IndianOrForeign { get; set; }

        public string Status { get; set; }

        public string CompanyType { get; set; }

    }

    public class Signatory : ISignatory
    {
        public Signatory() { }
        public Signatory(ProxySignatory signatory)
        {
            if (signatory == null)
                return;

            Din = signatory.Din;
            DirectorName = signatory.DirectorName;
            ResidentialAddress = signatory.ResidentialAddress;
            Designation = signatory.Designation;
            AppointmentDate = signatory.AppointmentDate;
            AppointmentDateOriginal = signatory.AppointmentDateOriginal;
            DateOfBirth = signatory.DateOfBirth;
            Age = signatory.Age;
            DirectorType = signatory.DirectorType;
        }
        public string Din { get; set; }

        public string DirectorName { get; set; }

        public string ResidentialAddress { get; set; }

        public string Designation { get; set; }

        public string AppointmentDate { get; set; }
        public string AppointmentDateOriginal { get; set; }
        public string DateOfBirth { get; set; }
        public string Age { get; set; }
        public string DirectorType { get; set; }
    }
    public class CompanyProfilePaid : ICompanyProfilePaid
    {
        public CompanyProfilePaid() { }
        public CompanyProfilePaid(ProxyCompanyProfilePaid profile)
        {
            if (profile == null)
                return;
            AuthorisedCapitalFormatted = profile.AuthorisedCapitalFormatted;
            paidUpCapitalFormatted = profile.paidUpCapitalFormatted;
            CompanyName = profile.CompanyName;
            Cin = profile.Cin;
            Pan = profile.Pan;
            TelephoneNumber = profile.TelephoneNumber;
            Website = profile.Website;
            RegisteredAddress = profile.RegisteredAddress;
            City = profile.City;
            State = profile.State;
            RegisteredAddressMap = profile.RegisteredAddressMap;
            Email = profile.Email;
            ClassOfCompany = profile.ClassOfCompany;
            ListedOrUnlisted = profile.ListedOrUnlisted;
            CompanyCategory = profile.CompanyCategory;
            CompanySubCategory = profile.CompanySubCategory;
            Status = profile.Status;
        }
        public string AuthorisedCapitalFormatted { get; set; }
        public string paidUpCapitalFormatted { get; set; }
        public string CompanyName { get; set; }
        public string Cin { get; set; }
        public string Pan { get; set; }
        public string TelephoneNumber { get; set; }
        public string Website { get; set; }
        public string RegisteredAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string RegisteredAddressMap { get; set; }
        public string Email { get; set; }
        public string ClassOfCompany { get; set; }
        public string ListedOrUnlisted { get; set; }
        public string CompanyCategory { get; set; }
        public string CompanySubCategory { get; set; }
        public string Status { get; set; }
    }

    public class FinancialSnapshotHistory : IFinancialSnapshotHistory
    {
        public FinancialSnapshotHistory() { }
        public FinancialSnapshotHistory(ProxyFinancialSnapshotHistory snapshot)
        {
            if (snapshot == null)
                return;

            ConsolidatedLongTermBorrowings = snapshot.ConsolidatedLongTermBorrowings;
            RevenueFromOperations = snapshot.RevenueFromOperations;
            Ebitda = snapshot.Ebitda;
            ProfitOrLoss = snapshot.ProfitOrLoss;
            Networth = snapshot.Networth;
            LongTermBorrowings = snapshot.LongTermBorrowings;
            AsOnDate = snapshot.AsOnDate;
            FinancialYear = snapshot.FinancialYear;
        }
        public long ConsolidatedLongTermBorrowings { get; set; }
        public long RevenueFromOperations { get; set; }
        public long Ebitda { get; set; }
        public long ProfitOrLoss { get; set; }
        public long Networth { get; set; }
        public long LongTermBorrowings { get; set; }
        public string AsOnDate { get; set; }
        public string FinancialYear { get; set; }
    }
}

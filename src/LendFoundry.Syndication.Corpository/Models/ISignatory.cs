﻿using System;

namespace LendFoundry.Syndication.Corpository
{
    public interface ISignatory
    {
        string Age { get; set; }
        string AppointmentDate { get; set; }
        string AppointmentDateOriginal { get; set; }
        string DateOfBirth { get; set; }
        string Designation { get; set; }
        string Din { get; set; }
        string DirectorName { get; set; }
        string DirectorType { get; set; }
        string ResidentialAddress { get; set; }
    }
}
﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Corpository
{
    public interface IHistoryCompany
    {
        int CompanyId { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ICompanyProfile, CompanyProfile>))]
        ICompanyProfile CompanyProfile { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ICompanyProfilePaid, CompanyProfilePaid>))]
        ICompanyProfilePaid CompanyProfilePaid { get; set; }
        List<object> ConsolidatedFinancialMaster { get; set; }
        List<object> ConsolidatedFinancialMasterHistories { get; set; }
        long LastDownloadedTime { get; set; }
        long LastRefreshTime { get; set; }
        string ClassOfCompany { get; set; }

        string ListingStatus { get; set; }

        string CompanyCategory { get; set; }

        string CompanySubCategory { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISignatory, Signatory>))]
        List<ISignatory> Signatories { get; set; }
    }
}
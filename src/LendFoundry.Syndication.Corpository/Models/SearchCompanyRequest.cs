﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Corpository
{
    public class SearchCompanyRequest : ISearchCompanyRequest
    {
        public List<string> Cins { get; set; }
        public List<string> CompanyIds { get; set; }

    }
}

﻿using LendFoundry.Syndication.Corpository.Proxy.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Corpository
{
    public class SearchCompanyResponse : ISearchCompanyResponse
    {
        public SearchCompanyResponse(){}

        public SearchCompanyResponse(SearchCompanyProxyResponse searchResponse )
        {
            if (searchResponse == null)
                return;
            Status = searchResponse.Status;
            Message = searchResponse.Message;
            //SearchSummary = searchResponse.SearchSummary;
            Companies = new List<Company>(searchResponse.Companies.Select(comapny => new Company(comapny)));
        } 
        public int Status { get; set; }
        public string Message { get; set; }
        public SearchSummary SearchSummary { get; set; }
     
        public List<Company> Companies { get; set; }
    }

    public class Company
    {
        public Company() { }
        public Company(ProxyCompany company)
        {
            if (company == null)
                return;
            CompanyName = company.CompanyName;
            Cin = company.Cin;
            CompanyId = company.CompanyId;
            City = company.City;
            State = company.State;
            City = company.City;
            State = company.State;
            CompanyStatus = company.CompanyStatus;
            LastRefreshTime = company.LastRefreshTime;
            DbView = company.DbView;
            DbCompany = company.DbCompany;
        }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string Cin { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string CompanyStatus { get; set; }
        public DateTime LastRefreshTime { get; set; }
        public bool DbCompany { get; set; }
        public bool DbView { get; set; }
        public bool PaidView { get; set; }
        public bool ProfileView { get; set; }
    }


    public class SearchSummary
    {
        public int TotalCount { get; set; }
        public int OffsetStart { get; set; }
        public int OffsetEnd { get; set; }
    }
}

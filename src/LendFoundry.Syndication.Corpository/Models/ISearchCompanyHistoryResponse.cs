﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Corpository
{
    public interface ISearchCompanyHistoryResponse
    {
        [JsonConverter(typeof(InterfaceListConverter<IHistoryCompany, HistoryCompany>))]
        List<IHistoryCompany> Companies { get; set; }
        string Message { get; set; }
        int Status { get; set; }
    }
}
﻿using System;

namespace LendFoundry.Syndication.Corpository
{
    public interface IFinancialSnapshotHistory
    {
        string AsOnDate { get; set; }
        long ConsolidatedLongTermBorrowings { get; set; }
        long Ebitda { get; set; }
        string FinancialYear { get; set; }
        long LongTermBorrowings { get; set; }
        long Networth { get; set; }
        long ProfitOrLoss { get; set; }
        long RevenueFromOperations { get; set; }
    }
}
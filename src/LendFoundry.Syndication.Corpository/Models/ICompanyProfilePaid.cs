﻿namespace LendFoundry.Syndication.Corpository
{
    public interface ICompanyProfilePaid
    {
        string AuthorisedCapitalFormatted { get; set; }
        string Cin { get; set; }
        string City { get; set; }
        string ClassOfCompany { get; set; }
        string CompanyCategory { get; set; }
        string CompanyName { get; set; }
        string CompanySubCategory { get; set; }
        string Email { get; set; }
        string ListedOrUnlisted { get; set; }
        string paidUpCapitalFormatted { get; set; }
        string Pan { get; set; }
        string RegisteredAddress { get; set; }
        string RegisteredAddressMap { get; set; }
        string State { get; set; }
        string Status { get; set; }
        string TelephoneNumber { get; set; }
        string Website { get; set; }
    }
}
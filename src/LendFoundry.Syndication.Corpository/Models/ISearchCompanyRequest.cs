﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Corpository
{
    public interface ISearchCompanyRequest
    {
        List<string> CompanyIds { get; set; }
        List<string> Cins { get; set; }
    }
}
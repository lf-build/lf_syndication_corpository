﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Syndication.Corpository
{
    public interface ICorpositoryConfiguration : IDependencyConfiguration
    {
         string UserName { get; set; }
         string Password { get; set; }
         string IntegratorKey { get; set; }
         string HostName { get; set; }
         string AppName { get; set; }
         string ReturnUrl { get; set; }
         string ApiKey { get; set; }
         string SecretKey { get; set; }
         string LoginRequestName { get; set; }
         string SearchCompanyRequestName { get; set; }
        string SearchCompanyHistroyRequestName { get; set; }
         string Source { get; set; }

         string UserId { get; set; }

        List<string> Cards { get; set; }
        int RequestTimeout{get;set;}
        string ConnectionString { get; set; }


    }
}

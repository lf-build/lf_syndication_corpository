﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Syndication.Corpository
{
    public class CorpositoryConfiguration : ICorpositoryConfiguration , IDependencyConfiguration
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string IntegratorKey { get; set; }
        public string HostName { get; set; }
        public string AppName { get; set; }
        public string ReturnUrl { get; set; }
        public string ApiKey { get; set; }
        public string SecretKey { get; set; }
        public string LoginRequestName{get;set;}
        public string SearchCompanyRequestName { get; set; }
        public string SearchCompanyHistroyRequestName { get; set; }
        public string Source { get; set; }

        public string UserId { get; set; }
        public List<string> Cards { get; set; }
        public int RequestTimeout{get;set;}
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
        
    }
}

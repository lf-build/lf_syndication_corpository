﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Syndication.Corpository
{
    [Serializable]
    public class CorpositoryException :Exception
    {
            public string ErrorCode { get; set; }

            public CorpositoryException()
            {
            }
            public CorpositoryException(string message) : this(null, message, null)
            {
            }

            public CorpositoryException(string errorCode, string message) : this(errorCode, message, null)
            {
            }

            public CorpositoryException(string errorCode, string message, Exception innerException) : base(message, innerException)
            {
                ErrorCode = errorCode;
            }
            public CorpositoryException(string message, Exception innerException) : this(null, message, innerException)
            {
            }

            protected CorpositoryException(SerializationInfo info, StreamingContext context) : base(info, context)
            {
            }
        }
    }


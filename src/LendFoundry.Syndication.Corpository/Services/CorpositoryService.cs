﻿using LendFoundry.Syndication.Corpository.Proxy;
using System;
using LendFoundry.Foundation.Date;
using System.Threading.Tasks;
using LendFoundry.Syndication.Corpository.Proxy.Request;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Corpository.Events;

namespace LendFoundry.Syndication.Corpository
{
    public class CorpositoryService : ICorpositoryService
    {
        public CorpositoryService(ICorpositoryProxy proxy, ICorpositoryConfiguration configuration, ITenantTime tenantTime, IEventHubClient eventHub, Foundation.Logging.ILogger logger)
        {
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));

            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            Configuration = configuration;
            CorpositoryProxy = proxy;
            TenantTime = tenantTime;
            EventHub = eventHub;
            Logger = logger;
        }


        #region Private Properties
        private ICorpositoryConfiguration Configuration { get; set; }
        private ICorpositoryProxy CorpositoryProxy { get; set; }
        private ITenantTime TenantTime { get; set; }
        private IEventHubClient EventHub { get; set; }
        private Foundation.Logging.ILogger Logger { get; }
        #endregion

        public async Task<ISearchCompanyResponse> SearchCompany(string entityType, string entityId, ISearchCompanyRequest request)
        {
            try
            {
                Logger.Info("Started Execution for SearchCompany Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "TypeOf Corpository: CorpositorySearch");

                if (request == null)
                    throw new ArgumentException("Value cannot be null", nameof(request));

                var searchRequest = new SearchCompanyProxyRequest
                {
                    RequestType = Configuration.SearchCompanyRequestName,
                    Parameters = new ProxyParameter
                    {
                        ApiKey = Configuration.ApiKey,
                        SecretKey = Configuration.SecretKey,
                        SourceSystem = Configuration.Source,
                        UserId = Configuration.UserId,
                        SearchCriteria = new ProxySearchCriteria
                        {
                            Cins = request.Cins
                        }
                    }
                };
               
                var companyDetails =  new SearchCompanyResponse(await CorpositoryProxy.SearchCompany(searchRequest));
                await EventHub.Publish(new CorpositorySearchCompanyCalled
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = companyDetails,
                    Request = request,
                    ReferenceNumber = null
                });
                Logger.Info("Completed Execution for Search Company Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Corpository: CorpositorySearch");
                return companyDetails;
            }
            catch (CorpositoryException ex)
            {
                Logger.Error($"The method Corpository Company Search({request}) raised an error: {ex.Message}");
                await EventHub.Publish(new CorpositorySearchCompanyRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = null
                });
                throw new NotFoundException($"Information not found for this request");
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message);
                await EventHub.Publish(new CorpositorySearchCompanyRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = null
                });
                throw ex;
            }
          
        }

        public async Task<ISearchCompanyHistoryResponse> SearchCompanyHistory(string entityType, string entityId, ISearchCompanyRequest request)
        {
            try
            {
                Logger.Info("Started Execution for Search Company Histroy Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "TypeOf Corpository: CorpositorySearch");

                if (request == null)
                    throw new ArgumentException("Value cannot be null", nameof(request));

                var searchRequest = new CompanyHistoryProxyRequest
                {
                    RequestType = Configuration.SearchCompanyHistroyRequestName,
                    Parameters = new ProxyHistoryParameter
                    {
                        cards = Configuration.Cards,
                        SecretKey = Configuration.SecretKey,
                        SourceSystem = Configuration.Source,
                        CompanyIds = request.CompanyIds
                    }
                };
                var companyHistory =  new SearchCompanyHistoryResponse(await CorpositoryProxy.SearchCompanyHistory(searchRequest));
                await EventHub.Publish(new CorpositorySearchCompanyHistoryPulled
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = companyHistory,
                    Request = request,
                    ReferenceNumber = null
                });
                Logger.Info("Completed Execution for Search Company History Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Corpository: CorpositorySearchHistory");
                return companyHistory;
            }
            catch (CorpositoryException ex)
            {
                Logger.Error($"The method Corpository Company Search({request}) raised an error: {ex.Message}");
                await EventHub.Publish(new CorpositorySearchCompanyHistoryRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = null
                });
                throw new NotFoundException($"Information not found for this request");
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                await EventHub.Publish(new CorpositorySearchCompanyHistoryRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = null
                });
                throw ex;
            }
        }
    }
}

﻿using System.Threading.Tasks;

namespace LendFoundry.Syndication.Corpository
{
    public interface ICorpositoryService
    {
        Task<ISearchCompanyResponse> SearchCompany(string entityType, string entityId, ISearchCompanyRequest cins);
        Task<ISearchCompanyHistoryResponse> SearchCompanyHistory(string entityType, string entityId, ISearchCompanyRequest cins);
    }
}

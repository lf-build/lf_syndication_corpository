﻿using System;
using LendFoundry.Syndication.Corpository.Proxy.Response;
using LendFoundry.Syndication.Corpository.Proxy.Request;
using RestSharp;
using System.Net;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Corpository.Proxy
{
    public class CorpositoryProxy : ICorpositoryProxy
    {
        #region Properties
        private ICorpositoryConfiguration Configuration { get; set; }
        public CorpositoryProxy(ICorpositoryConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            Configuration = configuration;
        }
        #endregion
        #region Public Methods
        public async Task<SearchCompanyProxyResponse> SearchCompany(SearchCompanyProxyRequest request)
        {
            return await Task.Run(() =>
            {
                SearchCompanyProxyResponse searchResult;

                #region Validation
                if (request == null)
                    throw new ArgumentException(nameof(request));

                if (string.IsNullOrWhiteSpace(request.RequestType))
                    throw new ArgumentException(nameof(request.RequestType));

                if (request.Parameters == null)
                    throw new ArgumentException(nameof(request.Parameters));

                //if (string.IsNullOrWhiteSpace(request.Parameters.ApiKey))
                //    throw new ArgumentException(nameof(request.Parameters.ApiKey));

                if (string.IsNullOrWhiteSpace(request.Parameters.SecretKey))
                    throw new ArgumentException(nameof(request.Parameters.SecretKey));

                //if (string.IsNullOrWhiteSpace(request.Parameters.UserId))
                //    throw new ArgumentException(nameof(request.Parameters.UserId));

                if (request.Parameters.SearchCriteria == null)
                    throw new ArgumentException(nameof(request.Parameters.SearchCriteria));

                if (request.Parameters.SearchCriteria.Cins == null || request.Parameters.SearchCriteria.Cins.Count <= 0)
                    throw new ArgumentException(nameof(request.Parameters.SearchCriteria.Cins));
                #endregion

                var login = Login();
                if (login.Status == 1)
                {
                    request.Parameters.ApiKey = login.Data.ApiAuthToken;
                    request.Parameters.UserId = Convert.ToString(login.Data.UserId);

                    var client = new RestClient(Configuration.HostName.TrimEnd('/'));
                    var searchRequest = new RestRequest(Method.POST);
                    var jsonRequest = (JsonConvert.SerializeObject(request));
                    searchRequest.AddParameter("application/json", jsonRequest, ParameterType.RequestBody);

                    try
                    {
                        searchResult = ExecuteRequest<SearchCompanyProxyResponse>(client, searchRequest);
                        if (searchResult == null || searchResult.Status != 1)
                        {
                            throw new CorpositoryException(searchResult.Message);
                        }
                    }
                    catch (Exception)
                    {
                        throw new CorpositoryException("Failed to connect to service");
                    }

                }
                else
                {
                    throw new CorpositoryException("Authentication Failed");
                }
                return searchResult;
            });
        }

        public async Task<SearchCompanyHistoryProxyResponse> SearchCompanyHistory(CompanyHistoryProxyRequest request)
        {
            return await Task.Run(() =>
            {
                SearchCompanyHistoryProxyResponse searchResult;
                #region Validation
                if (request == null)
                    throw new ArgumentException(nameof(request));

                if (string.IsNullOrWhiteSpace(request.RequestType))
                    throw new ArgumentException(nameof(request.RequestType));

                if (request.Parameters == null)
                    throw new ArgumentException(nameof(request.Parameters));

                if (string.IsNullOrWhiteSpace(request.Parameters.SecretKey))
                    throw new ArgumentException(nameof(request.Parameters.SecretKey));

                if (request.Parameters.cards == null)
                    throw new ArgumentException(nameof(request.Parameters.cards));

                if (request.Parameters.CompanyIds == null || request.Parameters.CompanyIds.Count <= 0)
                    throw new ArgumentException(nameof(request.Parameters.CompanyIds));

                #endregion
                var login = Login();
                if (login.Status == 1)
                {
                    request.Parameters.ApiKey = login.Data.ApiAuthToken;
                    request.Parameters.UserId = Convert.ToString(login.Data.UserId);

                    var client = new RestClient(Configuration.HostName.TrimEnd('/'));
                    var searchRequest = new RestRequest(Method.POST);

                    var jsonRequest = (JsonConvert.SerializeObject(request));
                    searchRequest.AddParameter("application/json", jsonRequest, ParameterType.RequestBody);

                    searchResult = ExecuteRequest<SearchCompanyHistoryProxyResponse>(client, searchRequest);
                    if (searchResult == null || searchResult.Status != 1)
                    {
                        throw new CorpositoryException(searchResult.Message);
                    }
                }
                else
                {
                    throw new CorpositoryException("Authentication Failed");
                }
                return searchResult;
            });
        }
        #endregion
        #region Private Methods

        private LoginResponse Login()
        {
            #region Validation
            if (string.IsNullOrWhiteSpace(Configuration.HostName))
                throw new ArgumentNullException(nameof(Configuration.HostName));

            if (string.IsNullOrWhiteSpace(Configuration.UserName))
                throw new ArgumentNullException(nameof(Configuration.UserName));

            if (string.IsNullOrWhiteSpace(Configuration.Password))
                throw new ArgumentNullException(nameof(Configuration.Password));

            if (string.IsNullOrWhiteSpace(Configuration.AppName))
                throw new ArgumentNullException(nameof(Configuration.AppName));
            #endregion

            //DocuSignLogin(request.Url, request.UserName, request.Password, request.IntegratorKey);
            //var envelopeSummary = CreateEnvelope(request);

            var client = new RestClient(Configuration.HostName.TrimEnd('/'));
            var loginRequest = new RestRequest(Method.POST);
            var Parameter = new Para()
            {
                UserName = Configuration.UserName,
                Password = Configuration.Password
            };
            var credentials = new LoginRequest
            {
                Request = Configuration.LoginRequestName,
                Parameter = Parameter

            };
            var jsonRequest = (JsonConvert.SerializeObject(credentials));
            loginRequest.AddParameter("application/json", jsonRequest, ParameterType.RequestBody);
            //loginRequest.AddBody(jsonRequest);
            var authResult = ExecuteRequest<LoginResponse>(client, loginRequest);
            if (authResult == null || authResult.Status != 1)
            {
                throw new CorpositoryException(authResult.Message);
            }
            return authResult;
        }

        private T ExecuteRequest<T>(IRestClient client, IRestRequest request) where T : class
        {
            var date = DateTime.UtcNow.ToString("ddd, d MMM yyyy HH:mm:ss") + " GMT";
            //var headerAuthKey = GenerateHeaderAuthKey(request.Method.ToString(), date, "/" + request.Resource, null);
            //request.AddHeader("Authorization", headerAuthKey);
            //request.AddHeader("Date", date);
            request.Timeout = Configuration.RequestTimeout;
            var response = client.Execute(request);

            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new CorpositoryException("Service call failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new CorpositoryException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");

            if (response.StatusCode.ToString().ToLower() == "methodnotallowed")
                throw new CorpositoryException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.StatusDescription ?? ""}");

            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new NotFoundException(response.Content);

            if (response.StatusCode != HttpStatusCode.OK)
                throw new CorpositoryException(
                    $"Service call failed. Status {response.StatusCode}. Response: {response.Content ?? ""}");
            try
            {
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response.Content, exception);
            }
        }
        #endregion

    }
}


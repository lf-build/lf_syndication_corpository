﻿using LendFoundry.Syndication.Corpository.Proxy.Request;
using LendFoundry.Syndication.Corpository.Proxy.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Corpository.Proxy
{
    public interface ICorpositoryProxy
    {
        Task<SearchCompanyProxyResponse> SearchCompany(SearchCompanyProxyRequest request);

        Task<SearchCompanyHistoryProxyResponse> SearchCompanyHistory(CompanyHistoryProxyRequest request);
    }
}

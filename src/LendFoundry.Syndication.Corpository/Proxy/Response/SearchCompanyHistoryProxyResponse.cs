﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Corpository.Proxy.Response
{
    public class SearchCompanyHistoryProxyResponse
    {
        [JsonProperty("status")]
        public int Status { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("companies")]
        public List<ProxyHistoryCompany> Companies { get; set; }
    }
    public class ProxyHistoryCompany
    {
        [JsonProperty("company-id")]
        public int CompanyId { get; set; }
        [JsonProperty("last-refresh-time")]
        public long LastRefreshTime { get; set; }
        [JsonProperty("last-downloaded-time")]
        public long LastDownloadedTime { get; set; }

        [JsonProperty("company-profile")]
        public ProxyCompanyProfile CompanyProfile { get; set; }
        [JsonProperty("signatories")]
        public List<ProxySignatory> Signatories { get; set; }
        //[JsonProperty("charges")]
        //public List<Charge> charges { get; set; }
        [JsonProperty("consolidated-financial-master")]
        public List<object> ConsolidatedFinancialMaster { get; set; }
        [JsonProperty("consolidated-financial-statement-histories")]
        public List<object> ConsolidatedFinancialMasterHistories { get; set; }
        [JsonProperty("company-profile-paid")]
        public ProxyCompanyProfilePaid CompanyProfilePaid { get; set; }

        [JsonProperty("financial-snapshot-histories")]
        public List<ProxyFinancialSnapshotHistory> FinancialSnapshotHistory { get; set; }
        
    }

    public class ProxyCompanyProfile
    {
        [JsonProperty("company-name")]
        public string CompanyName { get; set; }
        [JsonProperty("cin")]
        public string Cin { get; set; }
        [JsonProperty("registered-address")]
        public string RegisteredAddress { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("registered-address_map")]
        public string RegisteredAddressMap { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("class-of-company")]
        public string ClassOfCompany { get; set; }
        [JsonProperty("listed-or-unlisted")]
        public string ListedOrUnlisted { get; set; }
        [JsonProperty("company-category")]
        public string CompanyCategory { get; set; }
        [JsonProperty("company-sub-category")]
        public string CompanySubCategory { get; set; }
        [JsonProperty("authorised-capital")]
        public string AthorisedCapital { get; set; }
        [JsonProperty("paid-up-capital")]
        public string PaidUpCapital { get; set; }
        [JsonProperty("date-of-incorporation")]
        public string DateOfIncorporation { get; set; }
        [JsonProperty("indian-or-foreign")]
        public string IndianOrForeign { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("company-type")]
        public string CompanyType { get; set; }

    }

    public class ProxySignatory
    {
        [JsonProperty("din")]
        public string Din { get; set; }
        [JsonProperty("director-name")]
        public string DirectorName { get; set; }
        [JsonProperty("residential-address")]
        public string ResidentialAddress { get; set; }
        [JsonProperty("designation")]
        public string Designation { get; set; }
        [JsonProperty("appointment-date")]
        public string AppointmentDate { get; set; }
        [JsonProperty("appointment-date-original-value")]
        public string AppointmentDateOriginal { get; set; }
        [JsonProperty("date-of-birth")]
        public string DateOfBirth { get; set; }
        [JsonProperty("director-age")]
        public string Age { get; set; }
        [JsonProperty("director-type")]
        public string DirectorType { get; set; }
    }
    public class ProxyCompanyProfilePaid
    {
        [JsonProperty("company-type")]
        public string AuthorisedCapitalFormatted { get; set; }

        [JsonProperty("paidUpCapitalFormatted")]
        public string paidUpCapitalFormatted { get; set; }

        [JsonProperty("company-name")]
        public string CompanyName { get; set; }

        [JsonProperty("cin")]
        public string Cin { get; set; }

        [JsonProperty("pan")]
        public string Pan { get; set; }
        [JsonProperty("telephone-number-with-std-code")]
        public string TelephoneNumber { get; set; }
        [JsonProperty("website")]
        public string Website { get; set; }
        [JsonProperty("registered-address")]
        public string RegisteredAddress { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("registered-address_map")]
        public string RegisteredAddressMap { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("class-of-company")]
        public string ClassOfCompany { get; set; }
        [JsonProperty("listed-or-unlisted")]
        public string ListedOrUnlisted { get; set; }

        [JsonProperty("company-category")]
        public string CompanyCategory { get; set; }
        [JsonProperty("company-sub-category")]
        public string CompanySubCategory { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }

    }

    public class ProxyFinancialSnapshotHistory
    {
        [JsonProperty("consolidated-long-term-borrowings")]
        public long ConsolidatedLongTermBorrowings { get; set; }
        [JsonProperty("revenue-from-operations")]
        public long RevenueFromOperations { get; set; }
        [JsonProperty("ebitda")]
        public long Ebitda { get; set; }
        [JsonProperty("profit-or-(loss)")]
        public long ProfitOrLoss { get; set; }
        [JsonProperty("networth")]
        public long Networth { get; set; }
        [JsonProperty("long-term-borrowings")]
        public long LongTermBorrowings { get; set; }
        [JsonProperty("as-on-date")]
        public string AsOnDate { get; set; }
        [JsonProperty("financial-year")]
        public string FinancialYear { get; set; }
    }
}


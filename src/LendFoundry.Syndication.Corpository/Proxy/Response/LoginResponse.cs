﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Corpository.Proxy.Response
{
    public class LoginResponse
    {
        [JsonProperty("status")]
        public int Status { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("data")]
        public Data Data { get; set; }
    }

    public class Data
    {
        [JsonProperty("user_id")]
        public int UserId { get; set; }
        [JsonProperty("api_auth_token")]
        public string ApiAuthToken { get; set; }
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("email_id")]
        public string EmailId { get; set; }
        [JsonProperty("unread_notification_count")]
        public int? UnreadNotificationCount { get; set; }
        [JsonProperty("available_tabs")]
        public List<string> AvailableTabs { get; set; }

    }
}

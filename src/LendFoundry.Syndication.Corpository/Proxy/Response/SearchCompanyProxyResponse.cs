﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Corpository.Proxy.Response
{
    public class SearchCompanyProxyResponse
    {
        [JsonProperty("status")]
        public int Status { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("search-summary")]
        public ProxySearchSummary SearchSummary { get; set; }
        [JsonProperty("companies")]
        public List<ProxyCompany> Companies { get; set; }
    }

    public class ProxyCompany
    {
        [JsonProperty("company-id")]
        public int CompanyId { get; set; }
        [JsonProperty("company-name")]
        public string CompanyName { get; set; }
        [JsonProperty("cin")]
        public string Cin { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("company-status")]
        public string CompanyStatus { get; set; }
        [JsonProperty("last-refresh-time")]
        public DateTime LastRefreshTime { get; set; }
        [JsonProperty("dbcompany")]
        public bool DbCompany { get; set; }
        [JsonProperty("dbview")]
        public bool DbView { get; set; }
        [JsonProperty("paidview")]
        public bool PaidView { get; set; }
        [JsonProperty("profileview")]
        public bool ProfileView { get; set; }
    }

    public class ProxySearchSummary
    {
        [JsonProperty("total-count")]
        public int TotalCount { get; set; }
        [JsonProperty("offset-start")]
        public int OffsetStart { get; set; }
        [JsonProperty("offset-end")]
        public int OffsetEnd { get; set; }
    }

}

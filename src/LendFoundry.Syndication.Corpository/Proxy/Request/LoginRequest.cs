﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Corpository.Proxy.Request
{
    public class Para
    {
        [JsonProperty("user-id")]
        public string UserName { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
    }

    public class LoginRequest
    {
        [JsonProperty("request")]
        public string Request { get; set; }
        [JsonProperty("para")]
        public Para Parameter { get; set; }
    }
}

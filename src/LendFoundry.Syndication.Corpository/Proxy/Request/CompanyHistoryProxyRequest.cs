﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Corpository.Proxy.Request
{

    public class CompanyHistoryProxyRequest
    {
        [JsonProperty("request")]
        public string RequestType { get; set; }
        [JsonProperty("para")]
        public ProxyHistoryParameter Parameters { get; set; }
    }

    public class ProxyHistoryParameter
    {
        [JsonProperty("api_auth_token")]
        public string ApiKey { get; set; }
        [JsonProperty("user_id")]
        public string UserId { get; set; }
        [JsonProperty("secret_key")]
        public string SecretKey { get; set; }
        [JsonProperty("source_system")]
        public string SourceSystem { get; set; }
        [JsonProperty("include-cards")]
        public List<string> cards { get; set; }

        [JsonProperty("company-ids")]
        public List<string> CompanyIds { get; set; }
    }

   
}

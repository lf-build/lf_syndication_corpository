﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Corpository.Proxy.Request
{
    public class SearchCompanyProxyRequest
    {
        [JsonProperty("request")]
        public string RequestType { get; set; }
        [JsonProperty("para")]
        public ProxyParameter Parameters { get; set; }
    }

    public class ProxyParameter
    {
        [JsonProperty("api_auth_token")]
        public string ApiKey { get; set; }
        [JsonProperty("user_id")]
        public string UserId { get; set; }
        [JsonProperty("secret_key")]
        public string SecretKey { get; set; }
        [JsonProperty("source_system")]
        public string SourceSystem { get; set; }
        [JsonProperty("search-criteria")]
        public ProxySearchCriteria SearchCriteria { get; set; }
    }

    public class ProxySearchCriteria
    {
        [JsonProperty("company-ids")]
        public List<string> CompanyIds { get; set; }
        [JsonProperty("company-names")]
        public List<string> CompanyNames { get; set; }
        [JsonProperty("company-name-partials")]
        public List<string> CompanyNamesPartials { get; set; }
        [JsonProperty("cins")]
        public List<string> Cins { get; set; }
        [JsonProperty("cin-partials")]
        public List<string> CinPartials { get; set; }
        [JsonProperty("partials-search-type")]
        public string PartialsSearchType { get; set; }
        [JsonProperty("offset-start")]
        public int OffsetStart { get; set; }
        [JsonProperty("offset-end")]
        public int OffsetEnd { get; set; }
    }
}

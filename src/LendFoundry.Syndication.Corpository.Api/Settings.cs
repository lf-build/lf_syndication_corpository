﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Syndication.Corpository.Api
{
    /// <summary>
    /// Settings
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// environmen variables
        /// </summary>
        /// <returns></returns>
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "corpository";
    }
}
﻿using LendFoundry.EventHub;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif

namespace LendFoundry.Syndication.Corpository.Api.Controller
{
    /// <summary>
    /// ApiController
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// ApiController constructor
        /// </summary>
        /// <param name="service"></param>
        /// <param name="eventHub"></param>
        public ApiController(ICorpositoryService service, IEventHubClient eventHub)
        {
            Service = service;
            EventHub = eventHub;
        }
        private ICorpositoryService Service { get; }
        private IEventHubClient EventHub { get; }

        /// <summary>
        /// End point call the method to Search organization details
        /// </summary>
        /// <param name="entityType">type of Entity</param>
        /// <param name="entityId">entity Id</param>
        /// <param name="cin">contain attributes to perform request</param>
        /// <returns></returns>
        [HttpGet("{entitytype}/{entityid}/company/search/{*cin}")]
        [ProducesResponseType(typeof(ISearchCompanyResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> SearchCompany(string entityType, string entityId,string cin)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(cin))
                        throw new ArgumentException("Value cannot be null or whitespace.", nameof(cin));

                    cin = WebUtility.UrlDecode(cin);
                    var cinList = SplitCins(cin);

                    var searchCompanyRequest = new SearchCompanyRequest { Cins = cinList };
                    var result = await Service.SearchCompany(entityType, entityId, searchCompanyRequest);
                    return Ok(result);
                }
                catch (CorpositoryException ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// End point call the method to get the report for organization details
        /// it contains efxId and candid in request additionally.
        /// </summary>
        /// <param name="entityType">type of Entity</param>
        /// <param name="entityId">entity Id</param>
        /// <param name="companyid">contain attributes to perform request</param>
        /// <returns></returns>
        [HttpGet("{entitytype}/{entityid}/company/search/history/{*companyid}")]
        [ProducesResponseType(typeof(ISearchCompanyHistoryResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetCompanyHistroy(string entityType, string entityId, string companyid)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(companyid))
                        throw new ArgumentException("Value cannot be null or whitespace.", nameof(companyid));

                    companyid = WebUtility.UrlDecode(companyid);
                    var companyids = SplitCins(companyid);

                    var searchCompanyRequest = new SearchCompanyRequest { CompanyIds = companyids };
                    var result = await Service.SearchCompanyHistory(entityType, entityId, searchCompanyRequest);
                    return Ok(result);
                }
                catch (CorpositoryException ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        private static List<string> SplitCins(string cins)
        {
            return string.IsNullOrWhiteSpace(cins)
                ? new List<string>()
                : cins.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}

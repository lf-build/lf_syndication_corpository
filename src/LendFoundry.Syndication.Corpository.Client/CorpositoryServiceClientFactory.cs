﻿using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.Syndication.Corpository.Client
{
    public class CorpositoryServiceClientFactory : ICorpositoryServiceClientFactory
    {
        #region Public Constructors

        [Obsolete("Need to use the overloaded with Uri")]
        public CorpositoryServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public CorpositoryServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        #endregion Public Constructors

        #region Private Properties

        private string Endpoint { get; }
        private int Port { get; }
        private IServiceProvider Provider { get; }
        private Uri Uri { get; }
        #endregion Private Properties

        #region Public Methods

        public ICorpositoryService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("corpository");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new CorpositoryService(client);
        }

        #endregion Public Methods
    }
}
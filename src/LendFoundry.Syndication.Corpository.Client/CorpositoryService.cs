﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Corpository.Client
{
    /// <summary>
    /// class used for injecting IBBBSearch service.
    /// </summary>
    public class CorpositoryService : ICorpositoryService
    {
        #region Public Constructors

        public CorpositoryService(IServiceClient client)
        {
            Client = client;
        }
        #endregion Public Constructors

        #region Private Properties

        private IServiceClient Client { get; }
        #endregion Private Properties
        public async Task<ISearchCompanyResponse> SearchCompany(string entityType, string entityId, ISearchCompanyRequest requestParam)
        {
            IRestRequest request = new RestRequest("{entitytype}/{entityid}/company/search/{cin}", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            AppendCins(request, requestParam.Cins);
            return await Client.ExecuteAsync<SearchCompanyResponse>(request);
        }

        public async Task<ISearchCompanyHistoryResponse> SearchCompanyHistory(string entityType, string entityId, ISearchCompanyRequest requestParam)
        {
            IRestRequest request = new RestRequest("{entitytype}/{entityid}/company/search/history/{companyid}", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddUrlSegment("companyid", string.Join("/", requestParam.CompanyIds));
            return await Client.ExecuteAsync<SearchCompanyHistoryResponse>(request);
        }

        private static void AppendCins(IRestRequest request, IReadOnlyList<string> cins)
        {
            if (cins == null || !cins.Any())
                return;

            var url = request.Resource;
            for (var index = 0; index < cins.Count; index++)
            {
                var cin = cins[index];
                url = url + $"/{{cin{index}}}";
                request.AddUrlSegment($"cin{index}", cin);
            }
            request.Resource = url;
        }
    }
}
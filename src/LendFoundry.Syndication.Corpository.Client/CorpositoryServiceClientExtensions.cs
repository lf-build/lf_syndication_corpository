﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;

#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.Syndication.Corpository.Client
{
    /// <summary>
    /// all Service injection
    /// </summary>
    public static class CorpositoryServiceClientExtensions
    {
        #region Public Methods

        /// <summary>
        ///
        /// </summary>
        /// <param name="services">services</param>
        /// <param name="endpoint">endpoint of service</param>
        /// <param name="port">port of service</param>
        /// <returns>Service</returns>
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddCorpositoryService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<ICorpositoryServiceClientFactory>(p => new CorpositoryServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ICorpositoryServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        public static IServiceCollection AddCorpositoryService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<ICorpositoryServiceClientFactory>(p => new CorpositoryServiceClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<ICorpositoryServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddCorpositoryService(this IServiceCollection services)
        {
            services.AddSingleton<ICorpositoryServiceClientFactory>(p => new CorpositoryServiceClientFactory(p));
            services.AddSingleton(p => p.GetService<ICorpositoryServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        #endregion Public Methods
    }
}
﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.Corpository.Client
{
    public interface ICorpositoryServiceClientFactory
    {
        #region Public Methods

        ICorpositoryService Create(ITokenReader reader);

        #endregion Public Methods
    }
}